from setuptools import find_packages, setup
setup(
    name='INCLUDE',
    packages=find_packages(),
    version='0.1.0',
    description='Gates for the simulating the Schwinger theory on Qiskit',
    author='Niklas Mueller, Joseph A. Carolan, Andrew Connelly, Zohreh Davoudi, Eugene F. Dumitrescu, Kübra Yeter-Aydeniz',
    license='MIT',
)