from qiskit import *
from qiskit import Aer
from qiskit.circuit.library.standard_gates import HGate
from qiskit.transpiler import PassManager
from qiskit.transpiler.passes import Unroller
import numpy as np
import itertools
import matplotlib.ticker as ticker
from qiskit import *
from qiskit.visualization import plot_state_city
from qiskit.visualization import plot_histogram
from qiskit.providers.ibmq import least_busy
from qiskit.circuit import Parameter
from qiskit import IBMQ, assemble, transpile
import qiskit.quantum_info as qi
import scipy

from INCLUDE import physics

# The F Gate using convention 1 is occupied, 0 is unoccupied
def Fgate(k,N, dagger = False):
    qcirc = QuantumCircuit(2,name="NF_{:}".format(k))
    
    qcirc.p(-2.0*np.pi*k/N,1)
    qcirc.cx(1,0)
    qcirc.ch(0,1)
    qcirc.cx(1,0)
    qcirc.cz(1,0)
    
    if dagger:
        return qcirc.inverse()
    else:
        return qcirc

# The 2 CNOT fSWAP gate using convention 1 occupied, 0 unoccupied
def fSWAP():
    qcirc = QuantumCircuit(2,name="fSWAP")
    qcirc.rx(np.pi/2, 0)
    qcirc.rx(np.pi/2, 1)
    qcirc.cx(0, 1)
    qcirc.rx(np.pi/2, 0)
    qcirc.rz(np.pi/2, 1)
    qcirc.cx(0, 1)
    qcirc.rx(-np.pi/2, 0)
    qcirc.rx(-np.pi/2, 1)
    qcirc.rz(np.pi/2, 0)
    qcirc.rz(np.pi/2, 1)
    return(qcirc)

## Implements exp( -i/2*(thetax XX + thetay YY + thetaz ZZ))
def pauliExp(thetax = 0,thetay = 0,thetaz = 0, dagger=False):
    qcirc = QuantumCircuit(2,name="XX: {:}, YY: {:}, ZZ: {:}".format(thetax,thetay,thetaz))
    qcirc.cx(0,1)
    qcirc.rx(thetax,0)
    qcirc.rz(thetaz,1)
    qcirc.h(0)
    qcirc.cx(0,1)
    qcirc.rz(-thetay,1)
    qcirc.s(0)
    qcirc.h(0)
    qcirc.cx(0,1)
    qcirc.rx(-np.pi/2,0)
    qcirc.rx(np.pi/2,1)
    if(dagger):
        return(qcirc.inverse())

    return(qcirc)

## Implements exp( -i/2*(thetax XX + thetay YY + thetaz ZZ))
def partially_controlled_pauliExp(thetax = 0,thetay = 0,thetaz = 0, dagger=False):
    qcirc = QuantumCircuit(3,name="XX: {:}, YY: {:}, ZZ: {:}".format(thetax,thetay,thetaz))
    qcirc.cx(1,2) # Cnot unaffected
    qcirc.crx(thetax,0,1) # controlled x rotation
    qcirc.crz(thetaz,0,2) # controlled z
    qcirc.ch(0, 1)
    qcirc.cx(1, 2) # Cnot unaffected
    qcirc.crz(-thetay, 0, 2)
    qcirc.cp(np.pi/2, 0, 1)
    qcirc.ch(0, 1)
    qcirc.cx(1,2) # Cnot unaffected
    qcirc.crx(-np.pi/2, 0, 1)
    qcirc.crx(np.pi/2, 0, 2)
    if(dagger):
        return(qcirc.inverse())

    return(qcirc)

### Bogoliubov Transform B-Gate
# This goes from particle-antiparticle to even-odd
# This is the gate provided by Niklas, wrapped in nots
# Therefore, it acts on states in convention | 1 > occupied
def Bgate(q,N,m, dagger = False):
    alpha = physics.alpha(q, N, m)
    beta = physics.beta(q, N, m)
    
    alpha = -alpha
    beta = -beta
    
    qcirc = QuantumCircuit(2,name="B{:}".format(q))
#     # Transform to 1 occupied convention
#     for j in list(range(2)):
#         qcirc.x(j)
    
    # A^dagger --------------- #
    qcirc.rz(alpha,1)
    
    # B ---------------------- # 
    qcirc.x(1)
    qcirc.cx(1,0)
    qcirc.cry(2.0*beta,0,1)
    qcirc.cx(1,0)
    qcirc.x(1)
    
    # A ---------------------- #
    qcirc.rz(-alpha,1)

#     # Transform to 0 occupied convention
#     for j in list(range(2)):
#         qcirc.x(j)

    if(dagger):
        return(qcirc.inverse())

    return(qcirc)

# Gives disjoint, contiguous subintervals of length n from list lst
def chunks(lst, n):
    ### Yield successive n-sized chunks from lst.
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


# Appends gates from orderedList to qc, using the following convention:
# fSWAP = ["s",q1,q2]
# F-Gate = ["f",q1,q2,k]
# Fdagger-Gate = ["fd",q1,q2,k]
# B-Gate = ["b",q1,q2,k,m]
# Bdagger-Gate = ["b",q1,q2,k,m]
def listAppend(orderedlist,qc):
    N = len(qc.qubits)
    for i in orderedlist:
        if(i[0] == "s" and len(i) == 3):
            qc.append(fSWAP(),[i[1],i[2]])
        elif(i[0] == "f" and len(i) == 4):
            qc.append(Fgate(i[3],N,False),[i[1],i[2]])
        elif(i[0] == "fd" and len(i) == 4):
            qc.append(Fgate(i[3],N,True),[i[1],i[2]])
        elif(i[0] == "b" and len(i) == 5):
            qc.append(bogT(i[3],N,m,False),[i[1],i[2]])
        elif(i[0] == "bd" and len(i) == 5):
            qc.append(bogT(i[3],N,m,True),[i[1],i[2]])
        else:
            print("Failed circuit instruction: {:}".format(i))

# Permutes fermions from 0,1,2,3,4,5,6,7 --> 0,2,4,6,1,3,5,7
def swapladder(qubits,dagger = False):
    N = len(qubits)
    qcirc = QuantumCircuit(N,name="SwapLadder({:})".format(N))
    for particle in range(2, N, 2): # The qubit to bubble up
        dest_ind = particle//2
        swaplist = []
        for pos in range(particle, dest_ind, -1): # swap the qubit down to position
            swaplist += [["s", pos, pos - 1]]
        listAppend(swaplist, qcirc)
    if(dagger):
        return(qcirc.inverse())

    return(qcirc)