import INCLUDE
import qiskit
import numpy as np

# Changes basis of local particle/antiparticle state from eigenstates of Hamiltonian(+m) to eigenstates of Hamiltonian(-m)
# Performs Bdagger(q,N,+m) followed by B(q,N,-m)
# Not controlled, acts on two qubits, uses convention | 0 > occupied | 1 > unoccupied
# Mass assumed to be positive, Brillouin zone not specified in this method
def local_quench_gate(q, N, abs_m):
    qcirc = qiskit.QuantumCircuit(2, name="Reduced_Local_Quench{:}".format(q))
    alpha = INCLUDE.physics.alpha(q, N, abs_m)
    omega = INCLUDE.physics.dispersion(q, N, abs_m)
    beta = INCLUDE.physics.beta(q, N, abs_m)
    delta = INCLUDE.physics.delta(q, N, abs_m)
    theta2 = alpha - np.pi/2.0

    qcirc.x(0)
    qcirc.rz(-alpha - np.pi / 2, 1)
    qcirc.cx(0, 1)
    qcirc.rx(2 * beta - np.pi / 2, 0)
    qcirc.h(0)
    qcirc.cx(0, 1)
    qcirc.s(0)
    qcirc.h(0)
    qcirc.rz(np.pi / 2 - 2 * beta, 1)
    qcirc.cx(0, 1)
    qcirc.rx(-np.pi / 2, 0)
    qcirc.x(0)
    qcirc.rx(np.pi / 2, 1)
    qcirc.rz(alpha + np.pi / 2, 1)

    return qcirc

# Changes basis of global particle/antiparticle state from eigenstates of Hamiltonian(+m) to eigenstates of Hamiltonian(-m)
# Changes state of qubits 1,2,...,N, taken to be the physical qubits
# Uses a Bogoliubov with -N/4 <-> N/4 - 1 Brillouin zone, convention | 0 > occupied and | 1 > unoccupied
def quench_gate(N, abs_m, dagger=False):
    qubits = list(range(N))  # N physical qubits and 1 ancilla
    qcirc = qiskit.QuantumCircuit(len(qubits), name="Quench")
    qpairs = [[2 * a, 2 * a + 1] for a in range(N // 2)]
    BZ = range(-N//4, N//4)
    for q in BZ:
        q_ind = q - list(BZ)[0]
        local_quench = local_quench_gate(q, N, abs_m)
        qcirc.append(local_quench, qpairs[q_ind])
    if dagger:
        return qcirc.inverse()
    return qcirc

# Gate for Bogoliubov transformation, with conventions:
# Takes state in momentum basis to particle/antiparticle basis
# Uses a -N/4 <-> N/4 - 1 Brillouin zone
# hamiltonian with mass term m, spacing a=1, hopping term t = -0.5 (E-field irrelevant)
# qubits are all the physical qubits, convention | 0 > occupied and | 1 > unoccupied
def Bogoliubov_staggered_mom_to_particle_antiparticle(N, m, dagger=False):
    qubits = list(range(N))  # This gate acts on all the physical qubits
    BZ = list(range(-N//4, N//4))
    qcirc = qiskit.QuantumCircuit(N, name="B")
    # Define pairs of adjacent qubits to act on with B gate
    qpairs = [[int(2*a), int(2*a+1)] for a in range(int(N/2))]
    
    # added additional X gates
    for i_m in range(N//2):
        qcirc.x(2*i_m+1)
    
    for i in range(len(qpairs)):
        qcirc.append(INCLUDE.basic.Bgate(BZ[i], N, m, True), qpairs[i])
        
    if(dagger):
        return(qcirc.inverse())
    return(qcirc)

# Gate for Fermionic Fourier Fast transformation, with conventions:
# Takes state in momentum basis to position basis
# Uses a -N/4 <-> N/4 - 1 Brillouin zone
# hamiltonian with spacing a=1, hopping term t = -0.5, N=4 or 8 Physical Qubits (E-field, mass irrelevant)
# This routine should only be useful as a method in SFFFT_mom_to_pos
# qubits are all the physical qubits
# Convention is | 0 > unoccupied, | 1 > occupied, unless one_is_occupied=True is set
def FFFT_mom_to_pos(N_FFT, dagger=False, one_is_occupied=False):
    qubits = list(range(N_FFT))
    qcirc = qiskit.QuantumCircuit(N_FFT,name="FFFT_m2p(" + str(N_FFT) + ")")
    # Choose convention based on one_is_occupied flag
    for i in range(N_FFT):
        if not one_is_occupied:
            qcirc.x(i)

    # Hardcoding N=4 and N=2 case (so that SFFFT for N=8 and N=4, respectively, work) 
    
    # N=2 FFFT is just a F^0_2 (which is self-inverse, so the dagger does not matter)
    if N_FFT==2:
        qcirc.append(INCLUDE.basic.Fgate(0, 2, True),[0,1])

        # Re-set convention
        for i in range(N_FFT):
            if not one_is_occupied:
                qcirc.x(i)

        # If dagger is true, then this goes position to momentum
        if dagger:
            return qcirc.inverse()

        return qcirc
    
    # N=4 FFFT is as outlined in the note
    if N_FFT==4:
        qcirc.append(INCLUDE.basic.fSWAP(),[1,2])
        
        qcirc.append(INCLUDE.basic.Fgate(0, 4, True),[0,1])
        qcirc.append(INCLUDE.basic.Fgate(1, 4, True),[2,3])

        qcirc.append(INCLUDE.basic.fSWAP(),[1,2])
        
        qcirc.append(INCLUDE.basic.Fgate(0, 4, True),[0,1])
        qcirc.append(INCLUDE.basic.Fgate(0, 4, True),[2,3])
        
        qcirc.append(INCLUDE.basic.fSWAP(),[1,2])

        # Re-set convention
        for i in range(N_FFT):
            if not one_is_occupied:
                qcirc.x(i)
        
        # If dagger is true, then this goes position to momentum
        if dagger:
            return qcirc.inverse()

        return qcirc
        
    qcirc = qiskit.QuantumCircuit(N_FFT,name="FFFT_m2p(" + str(N_FFT) + ")")

    # Re-set convention
    for i in range(N_FFT):
        if not one_is_occupied:
            qcirc.x(i)

    return qcirc


# Gate for Staggered Fermionic Fast transformation, with conventions:
# Takes state in position basis to momentum basis
# Uses a -N/4 <-> N/4 - 1 Brillouin zone, | 0 > occupied and | 1 > unoccupied
# hamiltonian with spacing a=1, hopping term t = -0.5 (E-field, mass irrelevant)
# Assuming input is +-+-+-+-+-, accomplished by a Bog after (or Bog_dag before for inverse) whenever used
# This is the routine that should be used for our Schwinger simulation transformation
# qubits are all the physical qubits
def SFFFT_mom_to_pos(N, dagger=False):
    qubits = list(range(N))  # This acts on all the physical qubits
    qcirc = qiskit.QuantumCircuit(len(qubits), name=(
        "U_SFFFT_dg" if dagger else "U_SFFFT"))
    # Transform to 1 occupied convention
    for j in qubits:
        qcirc.x(j)

    particles = qubits[:int(N/2)]
    antiparticles = qubits[int(N/2):]

    qcirc.append(INCLUDE.basic.swapladder(qubits), qubits)

    # Particle FFFT, note that we are in | 1 > = occupied convention hence the flag
    qcirc.append(FFFT_mom_to_pos(len(particles), one_is_occupied=True), particles)

    # Antiparticle FFFT, note that we are in | 1 > = occupied convention hence the flag
    qcirc.append(FFFT_mom_to_pos(len(antiparticles), one_is_occupied=True), antiparticles)

    # Swapladder to re-arrange before Bogoliubov
    qcirc.append(INCLUDE.basic.swapladder(qubits, True), qubits)

    # Transform back to 0 occupied convention
    for j in qubits:
        qcirc.x(j)
        
    # Z gates to account for exp(i \pi m) phase ------------------------------ #
    for i_m in range(N//4):
        qcirc.z( 2 + i_m*4 )
        qcirc.z( 3 + i_m*4 )

    # If dagger is true, then this goes position to momentum
    if dagger:
        return qcirc.inverse()

    return qcirc
