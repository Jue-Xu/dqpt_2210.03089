import numpy as np
from INCLUDE.physics import dispersion
import qiskit
from qiskit import *

## Methods for Interacting Evolution
def d(n,m,N):
    return np.min([np.abs(n-m),N-np.abs(n-m)])

def v(d,N):
    if (d==0) | (d==1):
        return d
    elif (d==N//2):
        return (N**2-8)/(4*(N-3))
    else:
        return d + (d**2-3*d+2)/(3-N)

def compute_couplings(N,e,a,mass):
    Ezzs = np.zeros((N,N))
    for n in range(N):
        for k in range(N):
            Ezzs[n,k] = v(d(n,k,N),N)
    Ezzs = -0.25*(N-3)/(N-2)*Ezzs

    Ezz = [[((a*e)**2)*Ezzs[i,j],i,j] for i in range(N) for j in range(N)] # from gauss' law

    Ezs = np.zeros(N)
    for n in range(N):
        for m in range(N):
            if (m%2==1):
                Ezs[n] += v(d(n,m,N),N)+v(d(m,n,N),N)

    Ez = [[-e*e*a*a*Ezs[i],i] for i in range(N)]


    zz_terms_from_nn = []
    z_terms_from_nn = []

    for i in range(len(Ezz)):
        if np.abs(Ezz[i][0])>0.0:
            zz_terms_from_nn.append([0.25*Ezz[i][0],Ezz[i][1],Ezz[i][2]])
            z_terms_from_nn.append([0.25*Ezz[i][0],Ezz[i][1]])
            z_terms_from_nn.append([0.25*Ezz[i][0],Ezz[i][2]])

    z_terms_from_n = []

    for i in range(len(Ez)):
        if np.abs(Ez[i][0])>0.0:
            z_terms_from_n.append([0.5*Ez[i][0],Ez[i][1]])

    # --------------------------------- qiskit format -------------------------------------------- #
    z_couplings=np.zeros(N,float)
    zz_couplings=np.zeros((N,N),float)

    for i in range(len(z_terms_from_nn)):
        site=z_terms_from_nn[i][1]
        z_couplings[site]+=z_terms_from_nn[i][0]

    for i in range(len(z_terms_from_n)):
        site=z_terms_from_n[i][1]
        z_couplings[site]+=z_terms_from_n[i][0]


    for i in range(len(zz_terms_from_nn)):
        n1,n2=zz_terms_from_nn[i][1],zz_terms_from_nn[i][2]
        if n1 <= n2:
            zz_couplings[n1,n2]+=zz_terms_from_nn[i][0]
        else:
            zz_couplings[n2,n1]+=zz_terms_from_nn[i][0]
        
    return z_couplings,zz_couplings
# --------------------------------- END qiskit format -------------------------------------------- #

# -- Interaction part: simple z rotations controlled by qubit 0 -------------------------------- #
def interaction_z_part_controlled(N, my_dt, my_local_z_couplings, dagger = False):
    qcirc = qiskit.QuantumCircuit(N+1,name="I_z")
    
    #print("zz:",my_dt,my_local_z_couplings )
    
    for n in range(N):
        z_angle = 2.0*my_dt*(my_local_z_couplings[n])
        
        qcirc.crz(z_angle,0, n+1)
    
    if dagger==True:
        return qcirc.inverse()
    return qcirc
    
# -- Interaction part: zz rotations controlled by qubit 0 -------------------------------- #
def interaction_zz_part_controlled(N, my_dt, my_local_zz_couplings, dagger = False):
    qcirc = qiskit.QuantumCircuit(N+1,name="I_zz")    
    
    for n2 in range(0,N):
        for n1 in range(0,n2+1):
            
            if np.abs(my_local_zz_couplings[n1,n2])>0.0:
                zz_angle=2.0*my_dt*(my_local_zz_couplings[n1,n2])
                
                qcirc.ccx(0,n2+1,n1+1)
                qcirc.crz(zz_angle,0,n1+1)
                qcirc.ccx(0,n2+1,n1+1)
                
    if dagger==True:
        return qcirc.inverse()
    return qcirc

# -- Interaction part: all rotations controlled by qubit 0 -------------------------------- #
def interaction_part_controlled(N, my_dt, my_local_zz_couplings, my_local_z_couplings, dagger = False):
    qcirc = qiskit.QuantumCircuit(N + 1,name="Int")
    qubits = list(range(N + 1))
    
    qcirc.append(interaction_z_part_controlled(N, my_dt, my_local_z_couplings),qubits)
    qcirc.append(interaction_zz_part_controlled(N, my_dt, my_local_zz_couplings),qubits)

    if dagger==True:
        return qcirc.inverse()
    return qcirc

# -- Interaction part: simple z rotations -------------------------------- #
def interaction_z_part(N, my_dt, my_local_z_couplings, dagger = False):
    qcirc = qiskit.QuantumCircuit(N,name="I_z")
    
    #print("zz:",my_dt,my_local_z_couplings )
    
    for n in range(N):
        z_angle = 2.0*my_dt*(my_local_z_couplings[n])
        
        qcirc.rz(z_angle, n)
    
    if dagger==True:
        return qcirc.inverse()
    return qcirc
    
# -- Interaction part: zz rotations -------------------------------- #
def interaction_zz_part(N, my_dt, my_local_zz_couplings, dagger = False):
    qcirc = qiskit.QuantumCircuit(N,name="I_zz")    
    
    for n2 in range(0,N):
        for n1 in range(0,n2+1):
            
            if np.abs(my_local_zz_couplings[n1,n2])>0.0:
                zz_angle=2.0*my_dt*(my_local_zz_couplings[n1,n2])
                
                qcirc.cx(n2,n1)
                qcirc.rz(zz_angle,n1)
                qcirc.cx(n2,n1)
                
    if dagger==True:
        return qcirc.inverse()
    return qcirc

def interaction_part(N, my_dt, my_local_zz_couplings, my_local_z_couplings, dagger = False):
    qcirc = qiskit.QuantumCircuit(N,name="Int")
    qubits = list(range(N))
    
    qcirc.append(interaction_z_part(N, my_dt, my_local_z_couplings),qubits)
    qcirc.append(interaction_zz_part(N, my_dt, my_local_zz_couplings),qubits)

    if dagger==True:
        return qcirc.inverse()
    return qcirc