import INCLUDE.basic
import qiskit
import numpy as np

# Gives the absolute value of energy as a function of
# q - wave number, convention q is from 0 <-> N/2
# m - mass
# N - number of sites
# Lattice spacing a = 1 by convention
def dispersion(q, N, abs_m):
    w_q = np.sqrt(abs_m**2 + np.cos(alpha(q, N, abs_m))**2)
    return w_q

# Quantity alpha, defined in Bogoliubov transform notes
def alpha(q, N, abs_m):
    return 2.0*np.pi*q/N

# Quantity beta, defined in Bogoliubov transform notes
def beta(q, N, abs_m):
    w_q = dispersion(q, N, abs_m)
    return np.arctan2(np.sqrt((w_q-abs_m)/(2*w_q)),
                      np.sqrt((w_q+abs_m)/(2*w_q)))

# Quantity delta, defined in Bogoliubov transform notes
def delta(q, N, abs_m):
    w_q = dispersion(q, N, abs_m)
    return np.arctan2(np.sqrt((w_q+abs_m)/(2*w_q)),
                       np.sqrt((w_q-abs_m)/(2*w_q)))