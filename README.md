# Code repository: Quantum computation of dynamical quantum phase transitions and entanglement tomography in a lattice gauge theory

Niklas Mueller, Joseph A. Carolan, Andrew Connelly, Zohreh Davoudi, Eugene F. Dumitrescu, Kübra Yeter-Aydeniz
(https://arxiv.org/abs/2210.03089)

## License
Copyright 2023 Niklas Mueller, Joseph A. Carolan, Andrew Connelly, Zohreh Davoudi, Eugene F. Dumitrescu, Kübra Yeter-Aydeniz

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
IN THE SOFTWARE.

## Overview
This git repository contains all quantum circuits, and a basic set of analysis scripts from the project (https://arxiv.org/abs/2210.03089). Circuits are written in qiskit, and with interfaces such that they can be run locally  using the AER simulator (https://qiskit.org/documentation/tutorials/simulators/1_aer_provider.html). 

The code repository has three components, all circuits are explained in (https://arxiv.org/abs/2210.03089). We have added simple
analysis scripts along with the circuits, with some but not all of the functionallity of  (https://arxiv.org/abs/2210.03089), that allow a simple understanding of the codes.

### Loschmidt echo computation
### Correlation function and topological order parameter computation
### Entanglement computation through random measurements

## Getting started

Running this code requires a recent python and jupyter notebook installation, we recommend Anaconda. Installation of the required modules for using the IonQ machine can be found 
in the Google GCP and Microsoft Azure documentations.

